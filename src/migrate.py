'''
Created on Mar 30, 2013

@author: ankuragarwal
'''
import webapp2
import datastoretoolbox
import logging
import blobfile
from google.appengine.api import urlfetch
from google.appengine.ext import webapp


class MigrateData(webapp2.RequestHandler):
    def get(self):
        addresses = datastoretoolbox.dataFromDataStore(self)
        web_watch_name = self.request.get('web_watch_name')
        count = 0;
        for address in addresses:
            logging.info('migrating data from old data store to new format.')
            # Create a web address data store
            webAddress = datastoretoolbox.WebAddress(parent=datastoretoolbox.web_watch_key(web_watch_name))
            webAddress.author = address.author
            webAddress.schedule = address.schedule
            webAddress.webaddress = address.webaddress
            newWebAddress = webAddress.webaddress.replace("www.", "")
            result = urlfetch.fetch(newWebAddress)
            if (400 - result.status_code) <= 0:
                logging.error('Failed url fetch for ' + webAddress.webaddress)
            result.content = datastoretoolbox.removeHeaderInURLFetchContent(result.content)
            webAddress.similarityratio = datastoretoolbox.getSimilarityRatio(webAddress.webaddress, result.content)
            webAddress.contentkey = str(blobfile.createBlobFile(webAddress.author, webAddress.webaddress, result))
            webAddress.md5Hash = datastoretoolbox.getmd5CheckSumForKey(webAddress.contentkey)
            webAddress.put()
            #delete old blob file and web address
            blobfile.deleteBlobStoreWithKey(address.contentkey)
            address.delete()
            count += 1
            logging.info('Done ' + str(count) + ' migrations.')

app = webapp.WSGIApplication([('/tasks/migrate', MigrateData)],
                                     debug=True)
