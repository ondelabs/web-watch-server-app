'''
Created on 25 Mar 2013

@author: Ankur
'''
import webapp2
import logging
import smstwilio
from google.appengine.api import mail

class SendEmailNotification(webapp2.RequestHandler):
    def __init__(self, addressData, userDetail):
        self.addressData = addressData
        self.userDetail = userDetail
    
    def sendEmail(self, messageSubject, messageBody):
        emailAddress = self.userDetail.email
        if emailAddress is None:
            if "@" in self.addressData.author:
                emailAddress = self.addressData.author
        
        if emailAddress is not None:
            logging.info('email will be sent to ' + self.userDetail.email)
            message = mail.EmailMessage(sender="<akahank@gmail.com>",
                                subject=messageSubject)
    
            message.to = '<'+self.userDetail.email+'>'
            message.body = messageBody
            message.send()
    
    def sendChangeNotification(self):
        logging.info('Sending email to notify user ' + self.userDetail.email + ' that the web site at ' + self.addressData.webaddress + ' has changed.')
        self.sendEmail(self.addressData.webaddress + " has changed", """
        Hi """+self.addressData.author+""",
        
        The website ("""+self.addressData.webaddress+""") that you are watching has changed.
        
        The ondelabs.com Team
        """)
        # send a text message to the user's phone
        #if self.userDetail.phonenumber is not None:
            #smstwilio.sendSms(self.userDetail.phonenumber, self.addressData.webaddress+""" changed. The ondelabs.com Team""")
            
    def sendAllChangeNotifications(self, addresses):
        title = ""
        if len(addresses) > 1:
            title = "Websites have changed"
        else:
            title = "Website has changed"
        
        allAddress = ""
        if len(addresses) > 1:
            allAddress = allAddress + "The websites below have changed: \n\n"
        else:
            allAddress = allAddress + "The website below has changed: \n\n"
        
        addressData = None
        for address in addresses:
            logging.info('Sending email to notify user ' + self.userDetail.email + ' that the web site at ' + address.getAddressDetail().webaddress + ' has changed.')
            allAddress = allAddress + "         - " + address.getAddressDetail().webaddress + " \n"
            addressData = address.getAddressDetail()
        
        self.sendEmail(title, """
        Hi """+addressData.author+""",
        
        """ + allAddress + """
        
        The ondelabs.com Team
        """)
        
    def sendNewWebWatch(self):
        logging.info('Sending email to notify user ' + self.userDetail.email + ' they have added ' + self.addressData.webaddress + ' to their watch list')
        self.sendEmail("You've added " + self.addressData.webaddress + " to Web Watch", """
        Hi """+self.addressData.author+""",
        
        The website ("""+self.addressData.webaddress+""") has been added to your web site watch list.
        
        The ondelabs.com Team
        """)
        
    def sendRemovedWebWatch(self):
        logging.info('Sending email to notify user ' + self.userDetail.email + ' they have removed ' + self.addressData.webaddress + ' from their watch list')
        self.sendEmail("You've removed " + self.addressData.webaddress + " from Web Watch", """
        Hi """+self.addressData.author+""",
        
        The website ("""+self.addressData.webaddress+""") has been removed from your web site watch list.
        
        The ondelabs.com Team
        """)
        
    def sendErrorMessage(self, errorMessage):
        logging.info('Sending email to notify user ' + self.userDetail.email + ' with error message: ' + errorMessage)
        self.sendEmail("Error while watching " + self.addressData.webaddress, """
        Hi """+self.addressData.author+""",
        
        The website ("""+self.addressData.webaddress+""") has returned an error code.
        
        Error:
        """ + errorMessage + """
        
        The ondelabs.com Team
        """)
