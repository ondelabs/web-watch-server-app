'''
Created on 27 Mar 2013

@author: Ankur
'''
import webapp2
import jinja2
import os
import datastoretoolbox
import logging

from google.appengine.api import users
from src import smstwilio

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))


class AccountSettingsGet(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if user:
            if users.get_current_user():
                url = users.create_logout_url(self.request.uri)
                url_linktext = 'Logout'
            else:
                url = users.create_login_url(self.request.uri)
                url_linktext = 'Login'
            
            userDetail = datastoretoolbox.userDetailFromDataStore(self, user.nickname())
            
            error = self.request.get('error')
            
            template_values = {
            'url': url,
            'url_linktext': url_linktext,
            'phone_number': userDetail.phonenumber,
            'error': error,
            }
            
            template = jinja_environment.get_template('templates/accountsettings.html')
            self.response.out.write(template.render(template_values))
        else:
            self.redirect(users.create_login_url(self.request.uri))

def formatPhoneNumber(phoneNumber):
    if "+" not in phoneNumber:
        phoneNumber = "+"+phoneNumber
    
    return phoneNumber

class AccountSettingsError(webapp2.RequestHandler):
    def get(self):
        self.redirect('/accountsettings?error=An error has occurred. Please make sure it is a valid phone number')

class AccountSettingsPostPhoneNumber(webapp2.RequestHandler):
    def get(self):
        error = self.request.get('error')
        self.redirect('/accountsettings?error=' + error)
    
    def post(self):
        user = users.get_current_user()
        if user:
            phoneNumber = formatPhoneNumber(self.request.get('phonenumber'))
            if (phoneNumber != "+"):
                userDetail = datastoretoolbox.userDetailFromDataStore(self, user.nickname())
                if userDetail is None:
                    logging.warn("userDetail is None for user with email:" + user.email())
                else:
                    logging.info("About to set " + userDetail.author + "'s phone number to " + str(phoneNumber))
                    userDetail.phonenumber = str(phoneNumber)
                    userDetail.put()
                    error = smstwilio.sendSms(phoneNumber, 'You have registered this number to Web Watch. The ondelabs.com Team')
                    if "ERROR:" in error: 
                        self.redirect('/setphonenumber/error')
                        return
            self.redirect('/accountsettings?error=' + error)
        else:
            self.redirect('/')
