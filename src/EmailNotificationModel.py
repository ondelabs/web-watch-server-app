'''
Created on nov 22, 2013

@author: ankuragarwal
'''

class EmailNotificationModel(object):
    
    def __init__(self, addressDetail, userDetail):
        self._addressDetail = addressDetail
        self._userDetail = userDetail
        
    def getAddressDetail(self):
        return self._addressDetail
    
    def getUserDetail(self):
        return self._userDetail
