'''
Created on 21 Mar 2013

@author: Ankur
'''

import cgi
import cStringIO
import datetime
import logging
import urllib
import webapp2
import jinja2
import os
from random import randint

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

from google.appengine.ext import db
from google.appengine.api import memcache
from google.appengine.api import users


class Greeting(db.Model):
    """Models an individual Guestbook entry with an author, content, and date."""
    author = db.StringProperty()
    content = db.StringProperty(multiline=True)
    date = db.DateTimeProperty(auto_now_add=True)


def guestbook_key(guestbook_name=None):
    """Constructs a Datastore key for a Guestbook entity with guestbook_name."""
    return db.Key.from_path('Guestbook', guestbook_name or 'default_guestbook')


class MainGuestBookPage(webapp2.RequestHandler):
    def get(self):
        guestbook_name=self.request.get('guestbook_name')

        greetings = self.get_greetings(guestbook_name)
        stats = memcache.get_stats()

        self.response.out.write('<b>Memory Cache Hits:%s</b><br>' % stats['hits'])
        self.response.out.write('<b>Memory Cache Misses:%s</b><br><br>' %
                                stats['misses'])
        self.response.out.write(greetings)

        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        image = 'images/egg%i.jpg' % randint(1,3)

        template_values = {
                           'logoImage' : image,
                           'altLogoImage' : 'egg with pattern',
                           'logoImageText' : 'images/logo.jpg',
                           'altLogoImageText' : 'OndeLabs',
                            'greetings': greetings,
                            'url': url,
                            'url_linktext': url_linktext,
        }

        template = jinja_environment.get_template('templates/guestbook.html')
        self.response.out.write(template.render(template_values))
        
    def get_greetings(self, guestbook_name):
        """get_greetings()

        Checks the cache to see if there are cached greetings.
        If not, call render_greetings and set the cache

        Args:
          guestbook_name: Guestbook entity group key (string).

        Returns:
          A string of HTML containing greetings.
        """
        greetings = memcache.get('%s:greetings' % guestbook_name)
        if greetings is not None:
            return greetings
        else:
            greetings = self.render_greetings(guestbook_name)
            if not memcache.add('%s:greetings' % guestbook_name, greetings, 10):
                logging.error('Memcache set failed.')
            return greetings
            
    def render_greetings(self, guestbook_name):
        """render_greetings()

        Queries the database for greetings, iterate through the
        results and create the HTML.

        Args:
          guestbook_name: Guestbook entity group key (string).

        Returns:
          A string of HTML containing greetings
        """
        greetings = db.GqlQuery('SELECT * '
                                'FROM Greeting '
                                'WHERE ANCESTOR IS :1 '
                                'ORDER BY date DESC LIMIT 10',
                                guestbook_key(guestbook_name))
        return greetings


class Guestbook(webapp2.RequestHandler):
    def post(self):
    # We set the same parent key on the 'Greeting' to ensure each greeting is in
    # the same entity group. Queries across the single entity group will be
    # consistent. However, the write rate to a single entity group should
    # be limited to ~1/second.
        guestbook_name = self.request.get('guestbook_name')  # @UndefinedVariable
        greeting = Greeting(parent=guestbook_key(guestbook_name))

        if users.get_current_user():
            greeting.author = users.get_current_user().nickname()
            greeting.content = self.request.get('content')
            greeting.put()
            self.redirect('/guestbook?' + urllib.urlencode({'guestbook_name': guestbook_name}))

