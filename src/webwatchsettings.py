'''
Created on 22 Mar 2013

@author: Ankur
'''

from __future__ import with_statement
from google.appengine.api import urlfetch, users
import jinja2
import os
import urllib
import webapp2
import logging
import datastoretoolbox
import blobfile
import emailnotification
from random import randint

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

def formatWebAddress(address):
    newWebAddress = address
    if 'https://' not in newWebAddress:
        if 'http://' not in newWebAddress:
            logging.debug('https:// or http:// not in: ' + newWebAddress)
            if 'www.' not in newWebAddress:
                newWebAddress = 'www.' + newWebAddress
                logging.debug('added www. so now it is: ' + newWebAddress)
            if 'http://' not in newWebAddress:
                newWebAddress = 'http://' + newWebAddress
                logging.debug('added http:// so now it is: ' + newWebAddress)
    return newWebAddress

class WebWatch(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if user:
            if users.get_current_user():
                url = users.create_logout_url(self.request.uri)
                url_linktext = 'Logout'
            else:
                url = users.create_login_url(self.request.uri)
                url_linktext = 'Login'
            
            addresses = datastoretoolbox.dataFromDataStoreForUser(self, user)
            
            image = 'images/egg%i.jpg' % randint(1,3)
            
            template_values = {
                           'logoImage' : image,
                           'altLogoImage' : 'egg with pattern',
                           'logoImageText' : 'images/logo.jpg',
                           'altLogoImageText' : 'OndeLabs',
                            'url': url,
                            'addresses': addresses,
                            'url_linktext': url_linktext,
            }
            
            template = jinja_environment.get_template('templates/webwatchsettings.html')
            self.response.out.write(template.render(template_values))
        else:
            self.redirect(users.create_login_url(self.request.uri))


class AddWebWatch(webapp2.RequestHandler):
    def post(self):
    # We set the same parent key on the 'Greeting' to ensure each greeting is in
    # the same entity group. Queries across the single entity group will be
    # consistent. However, the write rate to a single entity group should
    # be limited to ~1/second.
        web_watch_name = self.request.get('web_watch_name')
        
        if users.get_current_user():
            newWebAddress = formatWebAddress(self.request.get('content'))
            if "http://www." != newWebAddress:
                addresses = datastoretoolbox.addressesFromDataStoreForUser(self, users.get_current_user())
                if datastoretoolbox.hasSimilar(self, addresses, newWebAddress) == False:
                    # Create a user detail data store, or retrieve it
                    userDetail = datastoretoolbox.userDetailFromDataStore(self, users.get_current_user().nickname())
                    userDetail.email = users.get_current_user().email()
                    userDetail.author = users.get_current_user().nickname()
                    userDetail.put()
                    # Create a web address data store
                    webAddress = datastoretoolbox.WebAddress(parent=datastoretoolbox.web_watch_key(web_watch_name))
                    webAddress.author = userDetail.author
                    webAddress.schedule = datastoretoolbox.ScheduleTime.hours24
                    newWebAddress = newWebAddress.replace("www.", "")
                    webAddress.webaddress = newWebAddress
                    result = urlfetch.fetch(webAddress.webaddress)
                    if (400 - result.status_code) <= 0:
                        emailnotification.SendEmailNotification(webAddress, userDetail).sendErrorMessage("An error code " + str(result.status_code) + " was returned while trying to watch " + webAddress.webaddress)
                    result.content = datastoretoolbox.removeHeaderInURLFetchContent(result.content)
                    webAddress.similarityratio = datastoretoolbox.getSimilarityRatio(webAddress.webaddress, result.content)
                    webAddress.contentkey = str(blobfile.createBlobFile(webAddress.author, webAddress.webaddress, result))
                    webAddress.md5Hash = datastoretoolbox.getmd5CheckSumForKey(webAddress.contentkey)
                    webAddress.put()
                    # notifying the user that they've added a web site to their list of sites to watch
                    emailnotification.SendEmailNotification(webAddress, userDetail).sendNewWebWatch()

        self.redirect('/webwatch?' + urllib.urlencode({'web_watch_name': web_watch_name}))

class RemoveWebWatch(webapp2.RequestHandler):
    def post(self):
        if users.get_current_user():
            newWebAddress = formatWebAddress(self.request.get('web_address'))
            if "http://www." != newWebAddress:
                addresses = datastoretoolbox.addressesFromDataStoreForUser(self, users.get_current_user())
                logging.info('about to delete a web address: ' + newWebAddress)
                if datastoretoolbox.hasSimilar(self, addresses, newWebAddress) == True:
                    self.deleteDataStoreFor(datastoretoolbox.dataFromDataStoreForUser(self, users.get_current_user()),
                                            datastoretoolbox.userDetailFromDataStore(self, users.get_current_user().nickname()),
                                            newWebAddress)
        self.redirect('/webwatch')
    
    def deleteDataStoreFor(self, addresses, userDetail, content):
        formattedWebAddress = formatWebAddress(content)
        logging.info('the web address has been formated to ' + formattedWebAddress)
        for address in addresses:
            if address.webaddress == formattedWebAddress:
                # notifying the user that they've removed the web site from their watch list
                emailnotification.SendEmailNotification(address, userDetail).sendRemovedWebWatch()
                # deleting the data store entry
                blobfile.deleteBlobStoreWithKey(address.contentkey)
                address.delete()

class ScheduleWebWatch(webapp2.RequestHandler):
    def get(self):
        self.redirect('/webwatch')
    
    def post(self):
        if users.get_current_user():
            newWebAddress = formatWebAddress(self.request.get('web_address'))
            newSchedule = datastoretoolbox.ScheduleTime.getClosestTime(int(self.request.get('schedule')))
            logging.info(users.get_current_user().nickname() + ' is rescheduling a watch for ' + newWebAddress + ' to ' + self.request.get('schedule') + ' minutes.')
            data = datastoretoolbox.dataFromDataStoreForUserWithWebAddress(self, users.get_current_user(), newWebAddress)
            if data is not None:
                data.schedule = newSchedule
                data.put()
            else:
                logging.warn('can\'t find the user ' + users.get_current_user().nickname() + ' who is watching ' + newWebAddress)
        
        self.redirect('/webwatch')
