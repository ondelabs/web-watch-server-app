'''
Created on 25 Mar 2013

@author: Ankur
'''

from google.appengine.api import files, blobstore

import logging

def createBlobFile(userNickName, webAddress, urlFetchResult):
    editedWebAddress = webAddress
    editedWebAddress = editedWebAddress.replace('/', '')
    editedWebAddress = editedWebAddress.replace(':', '')
    editedWebAddress = editedWebAddress.replace('?', '')
    editedWebAddress = editedWebAddress.replace('#', '')
    editedWebAddress = editedWebAddress.replace('%', '')
    editedWebAddress = editedWebAddress.replace('&', '')
    editedWebAddress = editedWebAddress.replace('=', '')
    # Create the file
    file_name = files.blobstore.create(mime_type='application/octet-stream',_blobinfo_uploaded_filename=userNickName+'_'+editedWebAddress)
    #logging.debug('(A) Ankur: ' + file_name)
    # Open the file and write to it
    with files.open(file_name, 'a') as f:
        f.write(urlFetchResult.content)
    # Finalize the file. Do this before attempting to read it.
    files.finalize(file_name)
    # Get the file's blob key
    blob_key = files.blobstore.get_blob_key(file_name)
    #logging.debug('(B) Ankur: ' + str(blob_key))
    #logging.debug('(C) Ankur: ' + urlFetchResult.content)
    return blob_key

def readBlobFile(key):
    f = None
    try:
#         logging.debug('(A) Ankur: ' + str(key))
        file_name = files.blobstore.get_file_name(blobstore.BlobKey(key))
#         logging.debug('(B) Ankur: ' + file_name)
        f = files.open(file_name, 'r')
#         logging.debug('(C) Ankur: ' + f.read())
    except:
        f = None
        logging.info("Could not retrieve the blob file data with key " + str(key))
    return f

def deleteBlobStoreWithKey(key):
    logging.info("Deleteing blob file with key " + str(key))
    blobstore.delete(key)
