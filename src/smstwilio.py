'''
Created on 27 Mar 2013

@author: Ankur
'''
from twilio.rest import TwilioRestClient

import logging

account = "AC6a108cec99f054bea6d59011a9e8e879"
token = "602b885fe056b01fe836cec29bcfd35b"
client = TwilioRestClient(account, token)
fromPhoneNumber = "+44 1908 410281"

def sendSms(phoneNumber, message):
    logging.info('sending ' + phoneNumber + ' message: [' + message + ']')
    error = "Successfully added phone number"
    try:
        message = client.sms.messages.create(to=phoneNumber, from_=fromPhoneNumber, body=message)
    except Exception, e:
        logging.warn(str(e))
        error = ('ERROR: ' + str(e))
    finally:
        return error