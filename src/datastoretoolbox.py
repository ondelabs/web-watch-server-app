'''
Created on 25 Mar 2013

@author: Ankur
'''
from google.appengine.ext import db
from google.appengine.api import urlfetch
import hashlib
import logging
import blobfile
import difflib
import math

class ScheduleTime():
    minutes10 = 10
    hour1 = 60
    hours12 = 720
    hours24 = 1440
    
    @staticmethod
    def getClosestTime(scheduledTime):
        minA = abs(ScheduleTime.minutes10 - scheduledTime)
        minB = abs(ScheduleTime.hour1 - scheduledTime)
        minC = abs(ScheduleTime.hours12 - scheduledTime)
        minD = abs(ScheduleTime.hours24 - scheduledTime)
        
        if minA < minB and minA < minC and minA < minD:
            return ScheduleTime.minutes10
        elif minB < minA and minB < minC and minB < minD:
            return ScheduleTime.hour1
        elif minC < minA and minC < minB and minC < minD:
            return ScheduleTime.hours12
        elif minD < minA and minD < minB and minD < minC:
            return ScheduleTime.hours24
        else:
            return ScheduleTime.hours24

class WebAddress(db.Model):
    author = db.StringProperty('author')
    webaddress = db.StringProperty('webaddress')
    contentkey = db.TextProperty('contentkey')
    md5Hash = db.TextProperty('md5hash')
    date = db.DateTimeProperty('date', auto_now_add=True)
    schedule = db.IntegerProperty('schedule')
    similarityratio = db.FloatProperty('similarityratio')
    
class UserDetails(db.Model):
    date = db.DateTimeProperty('date', auto_now_add=True)
    author = db.StringProperty('author')
    email = db.StringProperty('email')
    phonenumber = db.StringProperty('phonenumber')
    
def web_watch_key(web_watch_name=None):
    """Constructs a Datastore key for a WebWatch entity with guestbook_name."""
    return db.Key.from_path('WebWatch', web_watch_name or 'default_webWatch')

def user_detail_key(user_detail_name=None):
    return db.Key.from_path('UserDetails', user_detail_name or 'default_userDetails')

def createMD5Hash(data):
    m = hashlib.md5()
    m.update(str(data))
#     logging.debug('(D) Ankur: ' + m.hexdigest())
    return m.hexdigest()

def addressesFromDataStoreForUser(self, user):
    guestbook_name=self.request.get('guestbook_name')
    addresses = db.GqlQuery("SELECT * "
                            "FROM WebAddress "
                            "WHERE ANCESTOR IS :1 "
                            "ORDER BY date DESC",
                            web_watch_key(guestbook_name))
    userWebAddresses = []
    for address in addresses:
        if address.author == user.nickname() and address.webaddress is not None:
            userWebAddresses.append(address.webaddress)
            
    return userWebAddresses

def dataFromDataStore(self):
    guestbook_name=self.request.get('guestbook_name')
    addresses = db.GqlQuery("SELECT * "
                            "FROM WebAddress "
                            "WHERE ANCESTOR IS :1 "
                            "ORDER BY date DESC",
                            web_watch_key(guestbook_name))
    return addresses

def userDetailFromDataStore(self, nickname):
    user_details_name=self.request.get('user_details')
    details = db.GqlQuery("SELECT * "
                            "FROM UserDetails "
                            "WHERE ANCESTOR IS :1 "
                            "ORDER BY date DESC",
                            user_detail_key(user_details_name))
    for detail in details:
        if detail.author == nickname:
            return detail
            
    return UserDetails(parent=user_detail_key(user_details_name))

def dataFromDataStoreForUser(self, user):
    guestbook_name=self.request.get('guestbook_name')
    addresses = db.GqlQuery("SELECT * "
                            "FROM WebAddress "
                            "WHERE ANCESTOR IS :1 "
                            "ORDER BY date DESC",
                            web_watch_key(guestbook_name))
    userWebAddresses = []
    for address in addresses:
        if address.author == user.nickname() and address.webaddress is not None:
            userWebAddresses.append(address)
            
    return userWebAddresses

def dataFromDataStoreForUserWithWebAddress(self, user, webAddress):
    guestbook_name=self.request.get('guestbook_name')
    addresses = db.GqlQuery("SELECT * "
                            "FROM WebAddress "
                            "WHERE ANCESTOR IS :1 "
                            "ORDER BY date DESC",
                            web_watch_key(guestbook_name))
    for address in addresses:
        if address.author == user.nickname() and address.webaddress == webAddress:
            return address

    return None

def hasSimilar(self, addresses, webAddress):
    for address in addresses:
        if address == webAddress:
            return True
    return False

def getmd5CheckSumForKey(key):
    return createMD5Hash(blobfile.readBlobFile(key).read())

def removeHeaderInURLFetchContent(content):
    beginningIndex = content.find('<head>', 0, len(content))
    endIndex = content.find('</head>', beginningIndex, len(content))
    return content.replace(content[beginningIndex:endIndex+7], '')

def getmd5CheckSumForAddress(address):
    newWebAddress = address.replace("www.","")
    content = removeHeaderInURLFetchContent(urlfetch.fetch(newWebAddress).content)
    return createMD5Hash(content)

def getSimilarityRatio(webAddress, currentContent):
    oldRatio = 0
    sameCount = 0
    counter = 0;
    newWebAddress = webAddress.replace("www.","")
    while sameCount < 1:
        resultToCompare = urlfetch.fetch(newWebAddress)
        resultToCompare.content = removeHeaderInURLFetchContent(resultToCompare.content)
        ratio = math.floor(difflib.SequenceMatcher(None, currentContent, resultToCompare.content).ratio() * 100.0)/100.0
        logging.info('getSimilarityRatio ratio = ' + str(ratio) + ' oldRatio = ' + str(oldRatio))
        if ratio > 0.6:
            if ratio == oldRatio:
                sameCount += 1
            else:
                oldRatio = ratio
                sameCount = 0
        else:
            oldRatio = ratio
            sameCount = 0
            
        counter += 1
        if counter > 10:
            break
        
    return oldRatio

def isTheSameRatio(contentKey, newContent, originalRatio):
    if originalRatio == None:
        originalRatio = 1.0
    logging.debug("blob file key is " + contentKey)
    originalContent = blobfile.readBlobFile(contentKey)
    if originalContent is None:
        logging.info("originalContent is None")
        return False
    try:
        logging.info("originalContent is not None")
        readContent = originalContent.read()
        ratio = math.floor(difflib.SequenceMatcher(None, readContent, newContent).ratio() * 100.0)/100.0
        originalContent.close()
    except:
        logging.error("An error occurred while trying to read the blob file.")
        return False;
    logging.info('isTheSameRatio (in Task) ratio = ' + str(ratio) + ' originalRatio = ' + str(originalRatio))
    ratioDifference = ratio - originalRatio 
    if ratioDifference > 0.011:
        return False
    else:
        if ratioDifference < 0:
            return False
        else:
            return True
