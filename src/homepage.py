'''
Created on 21 Mar 2013

@author: Ankur
'''

import webapp2
import jinja2
import os
from random import randint

import guestbook
import webwatchsettings
import webwatchlogic
import accountsettings
import migrate

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

from google.appengine.api import users


'''TODO: Doesn't seem to delete the blob files when a website changes.'''
'''TODO: Fix error checks for twilio so that it's able to send error parameters in a path'''
'''TODO: Move some work to the backend - the comparisons can be moved to backend'''
'''TODO: Create a graph view to show when the website was the same as before and different'''

class HomePage(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if user:
            if users.get_current_user():
                url = users.create_logout_url(self.request.uri)
                url_linktext = 'Logout'
            else:
                url = users.create_login_url(self.request.uri)
                url_linktext = 'Login'
            
            image = 'images/egg%i.jpg' % randint(1,3)
            
            template_values = {
                           'logoImage' : image,
                           'altLogoImage' : 'egg with pattern',
                           'logoImageText' : 'images/logo.jpg',
                           'altLogoImageText' : 'OndeLabs',
                            'url': url,
                            'url_linktext': url_linktext,
            }
            
            template = jinja_environment.get_template('templates/index.html')
            self.response.out.write(template.render(template_values))
        else:
            self.redirect(users.create_login_url(self.request.uri))


app = webapp2.WSGIApplication([('/sign', guestbook.Guestbook),
                               ('/guestbook', guestbook.MainGuestBookPage),
                               ('/webwatch',webwatchsettings.WebWatch),
                              ('/addwebwatch',webwatchsettings.AddWebWatch),
                              ('/removewebwatch',webwatchsettings.RemoveWebWatch),
                              ('/schedulewebwatch',webwatchsettings.ScheduleWebWatch),
                              ('/accountsettings',accountsettings.AccountSettingsGet),
                              ('/setphonenumber/error',accountsettings.AccountSettingsError),
                              ('/setphonenumber',accountsettings.AccountSettingsPostPhoneNumber),
                              ('/tasks/migrate', migrate.MigrateData),
                              ('/tasks/processwebwatch10',webwatchlogic.ProcessWebWatch10),
                              ('/tasks/processwebwatch60',webwatchlogic.ProcessWebWatch60),
                              ('/tasks/processwebwatch720',webwatchlogic.ProcessWebWatch720),
                              ('/tasks/processwebwatch1440',webwatchlogic.ProcessWebWatch1440),
                              ('/', HomePage)],
                              debug=True)
