'''
Created on Mar 25, 2013

@author: ankuragarwal
'''

import threading
import time
import logging
import webapp2
import blobfile
from google.appengine.api import urlfetch
from google.appengine.ext import webapp

import datastoretoolbox
import emailnotification
from src.EmailNotificationModel import EmailNotificationModel

class ProcessWebWatch10(webapp2.RequestHandler):
    def get(self):
        startWebWatchCheckProcesses(self, datastoretoolbox.ScheduleTime.minutes10)
        
class ProcessWebWatch60(webapp2.RequestHandler):
    def get(self):
        startWebWatchCheckProcesses(self, datastoretoolbox.ScheduleTime.hour1)
        
class ProcessWebWatch720(webapp2.RequestHandler):
    def get(self):
        startWebWatchCheckProcesses(self, datastoretoolbox.ScheduleTime.hours12)
        
class ProcessWebWatch1440(webapp2.RequestHandler):
    def get(self):
        startWebWatchCheckProcesses(self, datastoretoolbox.ScheduleTime.hours24)
        
class doneFetch():
    def __init__(self, webAddress, fetchResult, md5CheckSum):
        self.webAddress = webAddress
        self.fetchResult = fetchResult
        self.md5CheckSum = md5CheckSum

def startWebWatchCheckProcesses(self, scheduleTime):
    logging.info('running web watch check process for tasks which run every ' + str(scheduleTime) + ' minutes.')
    addressData = datastoretoolbox.dataFromDataStore(self);
    threads = []
    count = 0
    doneList = []
    emailList = {}
    for address in addressData:
        if address.schedule == scheduleTime:
            # Create new thread
            thread = myThread(count, "Thread-" + str(count), address, datastoretoolbox.userDetailFromDataStore(self, address.author), doneList, emailList)
            # Start new Thread
            thread.start()
            # Add thread to thread list
            threads.append(thread)
            count = count + 1
    # Wait for all threads to complete
    for t in threads:
        t.join()
    sendEmails(emailList)
    logging.debug( "Exiting Main Thread" )
    
def sendEmails(emailList):
    if len(emailList.keys()) > 0:
        logging.debug("Sending emails")
        for key in emailList.keys():
            logging.debug("Sending email to " + key)
            value = emailList[key]
            if len(value) > 0:
                emailnotification.SendEmailNotification(value[0].getAddressDetail(), value[0].getUserDetail()).sendAllChangeNotifications(value)
            

class myThread (threading.Thread):
    threadLock = threading.Lock()
    
    def __init__(self, threadID, name, addressData, userDetail, doneList, emailList):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.addressData = addressData
        self.userDetail = userDetail
        self.doneList = doneList
        self.emailList = emailList
        
    def websiteFetched(self, webAddress):
        for doneFetchObject in self.doneList:
            if doneFetchObject.webAddress == webAddress:
                return doneFetchObject
        return None
        
    def run(self):
        # Get lock to synchronise threads
        self.threadLock.acquire()
        logging.info("Starting " + self.name + " to check " + self.addressData.webaddress)
        print_time(self.name)
        #check if another thread has already checked this website
        doneFetchObject = self.websiteFetched(self.addressData.webaddress)
        if doneFetchObject is not None:
            logging.info(self.addressData.webaddress + ' has already been checked, so reusing the result.')
            self.checkSimilarity(doneFetchObject.md5CheckSum, doneFetchObject.fetchResult)
        else:
            #Now check this website as it hasn't already been checked
            try:
                strippedWebAddress = self.addressData.webaddress.replace("www.", "")
                result = urlfetch.fetch(strippedWebAddress, deadline=15)
                if (400 - result.status_code) <= 0:
                    logging.info("Error code [" + str(result.status_code) + "] for " + self.addressData.webaddress)
                    emailnotification.SendEmailNotification(self.addressData, self.userDetail).sendErrorMessage("An error code " + str(result.status_code) + " was returned while trying to watch " + self.addressData.webaddress)
                else:
                    logging.info("Status code [" + str(result.status_code) + "] for " + self.addressData.webaddress)
                    result.content = datastoretoolbox.removeHeaderInURLFetchContent(result.content)
                    logging.debug("Header removed for " + self.addressData.webaddress)
                    md5CheckSum = datastoretoolbox.createMD5Hash(result.content)
                    logging.debug("MD5 created for " + self.addressData.webaddress)
                    self.doneList.append(doneFetch(self.addressData.webaddress, result, md5CheckSum))
                    self.checkSimilarity(md5CheckSum, result)
            except:
                logging.error("Could not complete fetch for " + self.addressData.webaddress)
        # Free lock to release next thread
        logging.info("Ending " + self.name)
        print_time(self.name)
        self.threadLock.release()
        
    def checkSimilarity(self, md5CheckSum, result):
        isSame = True
        if md5CheckSum != self.addressData.md5Hash:
            # First compare against the similarity ratio
            if datastoretoolbox.isTheSameRatio(self.addressData.contentkey, result.content, self.addressData.similarityratio) == False:
                isSame = False
                logging.info('both md5 checksums (new:' + md5CheckSum + ' target:' + self.addressData.md5Hash + ') are NOT the same for ' + self.addressData.webaddress)
                #The checksums do not match, so the web site has changed and the user needs to be notified
                key = self.getEmail(self.userDetail)
                if self.emailList.get(key) == None:
                    logging.debug("New email: " + key)
                    self.emailList[key] = [EmailNotificationModel(self.addressData, self.userDetail)]
                else:
                    logging.debug("Old email: " + key)
                    self.emailList[key].append(EmailNotificationModel(self.addressData, self.userDetail))
                #Update instance of datastore
                blobfile.deleteBlobStoreWithKey(self.addressData.contentkey)
                self.addressData.similarityratio = datastoretoolbox.getSimilarityRatio(self.addressData.webaddress, result.content)
                self.addressData.contentkey = str(blobfile.createBlobFile(self.addressData.author, self.addressData.webaddress, result))
                self.addressData.md5Hash = md5CheckSum
                self.addressData.put()

        if isSame == True:
            logging.info('both md5 checksums (new:' + md5CheckSum + ' target:' + self.addressData.md5Hash + ') are the same for ' + self.addressData.webaddress)
            
    def getEmail(self, userDetail):
        emailAddress = userDetail.email
        if emailAddress is None:
            if "@" in self.addressData.author:
                emailAddress = self.addressData.author
        return emailAddress

def print_time(threadName):
    logging.debug("%s: %s" % (threadName, time.ctime(time.time())))


app = webapp.WSGIApplication([('/tasks/processwebwatch10', ProcessWebWatch10),
                              ('/tasks/processwebwatch60', ProcessWebWatch60),
                              ('/tasks/processwebwatch720', ProcessWebWatch720),
                              ('/tasks/processwebwatch1440', ProcessWebWatch1440)],
                                     debug=True)
